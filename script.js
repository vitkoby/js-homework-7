'use strict';

/* Ответы на теоритические вопросы : 
1. Метод forEach нужен для последовательного перебора элементов массива.
2. 1) Можно указать длинну массива 0 (.length = 0); 2) с помощью метода arr.splice(0,arr.length)
3. Метод isArray() возращает true или false в зависимости от результата. 
*/

let arr = ['hello', 'world', 23, '23', null, [], 48, 'name', true];

const filterBy = arr.filter(function (val) {
    return !(typeof val == 'string');
});

console.log(filterBy);

